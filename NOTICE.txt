OPEN SOURCE SOFTWARE NOTICE

Please note we provide an open source software notice for the third party open source software along with this software and/or this software component (in the following just “this SOFTWARE”). The open source software licenses are granted by the respective right holders.

Warranty Disclaimer
THE OPEN SOURCE SOFTWARE IN THIS PRODUCT IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT WITHOUT ANY WARRANTY, WITHOUT EVEN THE IMPLIED WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE APPLICABLE LICENSES FOR MORE DETAILS.

Copyright Notice and License Texts

----------------------------------------------------------------------
Software: MMKV 1.0.7

Copyright notice:
Copyright (C) 2018 THL A29 Limited, a Tencent company.
Copyright (c) 1998-2018 The OpenSSL Project.  
Copyright (C) 1995-1998 Eric Young (eay@cryptsoft.com) 
Copyright (c) 1998-2018 The OpenSSL Project.  
Copyright (C) 2014 seven456@gmail.com
Copyright (C) 1995-2017 Jean-loup Gailly and Mark Adler

Licensed under the BSD 3-Clause License (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of
the License at https://opensource.org/licenses/BSD-3-Clause


